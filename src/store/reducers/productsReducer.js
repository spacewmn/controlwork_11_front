import {
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCT_INFO_SUCCESS,
    FETCH_PRODUCT_INFO_FAILURE,
    CREATE_NEW_PRODUCT_FAILURE,
}
    from "../actionTypes";

const initialState = {
    error: null,
    products:[],
    productInfo: null

};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case FETCH_PRODUCTS_FAILURE:
            return {...state, error: action.error};
        case FETCH_PRODUCT_INFO_SUCCESS:
            return {...state, productInfo: action.productInfo};
        case FETCH_PRODUCT_INFO_FAILURE:
            return {...state, error: action.error};
        case CREATE_NEW_PRODUCT_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default productsReducer;
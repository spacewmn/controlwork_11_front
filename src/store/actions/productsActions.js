import {
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCT_INFO_SUCCESS,
    FETCH_PRODUCT_INFO_FAILURE, CREATE_NEW_PRODUCT_SUCCESS, CREATE_NEW_PRODUCT_FAILURE
} from "../actionTypes";
import axios from "../../axiosApi";
import {push} from "connected-react-router";

const fetchProductsSuccess = (products) => {
    return {type: FETCH_PRODUCTS_SUCCESS, products}
};

const fetchProductsError = (error) => {
    return {type: FETCH_PRODUCTS_FAILURE, error}
};

export const fetchProducts = () => {
    return async dispatch => {
        try {
            const response = await axios.get("/products");
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductsError(e.response.data.error));
        }
    };
};

const fetchProductInfoSuccess = (productInfo) => {
    return {type: FETCH_PRODUCT_INFO_SUCCESS, productInfo}
};

const fetchProductInfoError = (error) => {
    return {type: FETCH_PRODUCT_INFO_FAILURE, error}
};

export const fetchProductInfo = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get("/products/" + id);
            console.log(response.data);
            dispatch(fetchProductInfoSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductInfoError(e.response.data.error));
        }
    };
};

const createNewProductSuccess = () => {
    return {type: CREATE_NEW_PRODUCT_SUCCESS};
};

const createNewProductError = (error) => {
    return {type: CREATE_NEW_PRODUCT_FAILURE,error};
};

export const createNewProduct = productData => {
    return async (dispatch, getState) => {
        const headers = {
            'Authorization': getState().users.user && getState().users.user.token
        };
        try {
            await axios.post("/products", productData,{headers} ).then(() => {
                dispatch(createNewProductSuccess());
                dispatch(push("/"));
            })}
        catch (e){
            dispatch(createNewProductError(e.response.data))
        }
    };
};
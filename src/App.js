import React from 'react';
import {Switch, Route} from "react-router-dom";
import {useSelector} from "react-redux";
import Layout from "./components/Layout/Layout";
// import Posts from "./containers/Posts/Posts";
// import FullPost from "./components/FullPost/FullPost";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
// import NewPost from "./containers/NewPost/NewPost";
import Products from "./containers/Products/Products";
import ProductInfo from "./components/ProductInfo/ProductInfo";
import NewProduct from "./containers/NewProduct/NewProduct";

const App = () => {
  const user = useSelector(state => state.users.user);
  return (
      <Layout user={user}>
        <Switch>
          <Route path="/" exact component={Products}/>
          <Route path="/products/add-product" exact component={NewProduct}/>
          <Route path="/products/:id" exact component={ProductInfo}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
        </Switch>
      </Layout>
  )
};

export default App;
import React from 'react';
import imageNotAvailable from "../../assets/images/no_image_available.jpeg";
import {apiURL} from '../../constants';
import {NavLink} from "react-router-dom";

const ProductItem = (props) => {
    let cardImage = imageNotAvailable ;

    if (props.image) {
        cardImage = apiURL + '/uploads/' + props.image;
    }
    return (
        <div className="card d-inline-block m-4" style={{"width": "18rem"}}>
            <img src={cardImage} className="card-img-top" alt="..."/>
                <div className="card-body">
                    <h5 className="card-title">{props.title}</h5>
                    <p className="card-text">{props.price}</p>
                    <NavLink to={'products/' + props.id} className="btn btn-primary">More</NavLink>
                </div>
        </div>
    );
};

export default ProductItem;
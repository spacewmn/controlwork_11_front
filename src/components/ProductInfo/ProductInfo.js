import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {apiURL} from "../../constants";
import imageNotAvailable from "../../assets/images/no_image_available.jpeg";
import {fetchProductInfo} from "../../store/actions/productsActions";

const ProductInfo = (props) => {
    const id = props.match.params.id;
    const dispatch = useDispatch();
    const productInfo = useSelector(state => state.products.productInfo);
    const error = useSelector(state => state.products.error);
    const user = useSelector(state => state.users.user);

    let cardImage = imageNotAvailable;
    if (productInfo.image) {
        cardImage = apiURL + '/uploads/' + productInfo.image;
    }

    useEffect(() => {
        dispatch(fetchProductInfo(id));
    }, [dispatch, id]);
    console.log(productInfo);


    return productInfo && (
        <div className="container">
            {error ? <b>{error}</b> :
                <>
                    <div className="card mt-3">
                        <div>
                            <img src={cardImage} className="card-img-top " alt="..."/>
                        </div>
                        <div className="card-body">
                            <h5 className="card-title mt-3">{productInfo.title}</h5>
                            <p>{productInfo.description}</p>
                            <h6>{productInfo.price} KGS</h6>
                            <p>{productInfo.category.title}</p>
                        </div>
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item">{productInfo.user.displayName}</li>
                            <li className="list-group-item">{productInfo.user.phoneNumber}</li>
                        </ul>
                    </div>
                </>
            }
        </div>
    );
};


export default ProductInfo;
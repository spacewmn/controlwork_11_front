import React from 'react';
import AnonymousMenu from "./AnonimousMenu";
import UserMenu from "./UserMenu";
import {NavLink} from "react-router-dom";

const Toolbar = (user) => {
    console.log(user);
    return (
        <>
            <nav className="navbar navbar-dark bg-success row">
                <div className="container">
                    <NavLink className="display-4 text-light col-8" to={"/"}>All products</NavLink>
                    {user.user == null ?
                        <AnonymousMenu />
                        :
                        <UserMenu user={user} />
                    }
                </div>
            </nav>
        </>
    );
};

export default Toolbar;
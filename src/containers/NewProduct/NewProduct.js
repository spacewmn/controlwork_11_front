import React from "react";
import {useDispatch} from "react-redux";
import {createNewProduct} from "../../store/actions/productsActions";
import NewProductForm from "../NewProductForm/NewProductForm";

const NewProduct = props => {
    const dispatch = useDispatch();

    const createProduct = productData => {
        dispatch(createNewProduct(productData)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3">Add new product</h1>
            <NewProductForm onSubmit={createProduct} />
        </div>
    );
};

export default NewProduct;
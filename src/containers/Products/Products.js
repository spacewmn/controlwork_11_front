import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductItem from "../../components/ProductItem/ProductItem";

const Products = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const error = useSelector(state => state.products.error);

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <>
            {error ? <b>{error}</b> :
                <div className="container">
                    {products.map(product => {
                        return (
                            <ProductItem
                                key={product._id}
                                id={product._id}
                                title={product.title}
                                price={product.price}
                                image={product.image}
                            />
                        )
                    })}
                </div>
            }
        </>
    );
};

export default Products;
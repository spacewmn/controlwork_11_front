import React, {useState} from "react";

const NewProductForm = ({onSubmit}) => {

    const [state, setState] = useState({
        title: "",
        description: "",
        image: "",
        price: ''
    });

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });

        console.log('TEST')
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    return (
        <div className="container bg-gradient-info">
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <input type="text"
                           className="form-control"
                           id="title"
                           placeholder="Title"
                           value={state.title}
                           onChange={inputChangeHandler}
                           name="title"
                           required/>
                </div>
                <div className="input-group mb-3">
                    <textarea className="form-control"
                              placeholder="Product"
                              onChange={inputChangeHandler}
                              name="description"
                              value={state.description}
                              required
                    />
                </div>
                <div className="input-group mb-3 ">
                    <label htmlFor="FormControlFile1">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        id="FormControlFile1"
                        onChange={fileChangeHandler}
                    />
                </div>
                <div className="input-group mb-3">
                    <input type="number"
                           className="form-control"
                           id="price"
                           placeholder="Price"
                           value={state.price}
                           onChange={inputChangeHandler}
                           name="price"
                           required/>
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                >Create new post
                </button>
            </form>
        </div>
    );
};

export default NewProductForm;